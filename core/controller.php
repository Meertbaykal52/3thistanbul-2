<?php

class Controller
{
    private $twig;
    public function __construct()
    {
        $loader = new Twig_Loader_Filesystem('views');
        $this->twig = new Twig_Environment($loader);
        $filter = new Twig_Filter('lang', 'Lang::get');
        $this->twig->addFilter($filter);
    }

    protected function view($viewname, $data = [])
    {
        $ext = !empty(extension) ? extension : '.html';
        if (!file_exists(__root__ . '/' . '/views/' . $viewname . $ext)) {
            echo 'Not found ' . $viewname;
            exit;
        }

        $data['__base__'] = __base__;
        $data['__env__'] = env;
        $data['__static__'] = env == 'development' ? '/www' : '';
        $data['__pathname'] = $_SERVER['REQUEST_URI'];

        $len = strpos($data['__pathname'], '?');
        if ($len) {
            $data['__pathname'] = substr($data['__pathname'], 0, $len);
        }

        echo $this->twig->render($viewname . $ext, $data);
    }
}
